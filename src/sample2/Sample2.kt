package sample2

fun sample1(): Int? {
    return 123
}

fun main(args: Array<String>) {
    val i: Int = sample1() ?: -1
}
