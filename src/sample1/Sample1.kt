package sample1

fun sample1(): Int {
    return 123
}

fun sample2(): Int {
    return Integer.valueOf(123)
}

fun sample3(): Integer {
    return Integer.valueOf(123) as Integer
}

fun main(args: Array<String>) {
    val s1: Int = sample1()
    val s2: Integer = sample2() as Integer
    val s3: Int = sample3() as Int
}
