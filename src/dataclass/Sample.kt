package dataclass

data class Sample(
        val property: Int,
        val nullable: String? = null) {

    val computed: String?
        get() = nullable?.toUpperCase()

}
