package sample3

fun sample1(): Int {
    val list = listOf(1, 2, 3)
    return list[0]
}

fun sample2(): Int? {
    val list = listOf<Int?>(1, 2, 3)
    return list[0]
}
